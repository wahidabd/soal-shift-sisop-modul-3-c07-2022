# soal-shift-sisop-modul-3-C07-2022

## Anggota Kelompok ##

NRP | NAMA
------------- | -------------
5025201039  | Abd. Wahid
5025201056  | Rendi Dwi Francisko
5025201239  | Kadek Ari Dharmika

# Soal 1
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

a.Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

b.Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

c.Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

d.Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

e.Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
# Catatan :
- Boleh menggunakan execv(), tapi bukan mkdir, atau system
- Jika ada pembenaran soal, akan di-update di catatan
- Kedua file .zip berada di folder modul
Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak. Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka. Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka
- Menggunakan apa yang sudah dipelajari di Modul 3 dan, kalau  perlu, di Modul 1 dan 2

### A
#### Download dan unzip file
```
void download_file(){
    char *files[] = {
        "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "music.zip",
        "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "quote.zip"
    };

    int status;
    pid_t child = fork();

    if(child < 0) exit(EXIT_FAILURE);

    for (int i = 0; i < 4; i += 2){
        if(child == 0){
            char *argv[] = {"wget", "-q", "--no-check-certificate", files[i], "-O", files[i + 1], NULL};
            execv("/bin/wget", argv);
        }
        while((wait(&status)) > 0);
        child = fork();
    }
}

void *unzip_or_create_dir(void *args) {
    char *argv1[] = {"unzip", "-o", "music.zip", "-d", "music", NULL};
    char *argv2[] = {"unzip", "-o", "quote.zip", "-d", "quote", NULL};
    char *argv3[] = {"mkdir", "-p", "hasil", NULL};


    pthread_t id = pthread_self();
    int status;

    if(pthread_equal(id, thread[1])) {
        printf("-----UNZIP MUSIC------\n");
        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv1);
        }
    }

    if(pthread_equal(id, thread[2])) {
        printf("-----UNZIP QUOTE------\n");
        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv2);
        }
    }

    if(pthread_equal(id, thread[3])) {
        printf("-----CREATE FILE------\n");
        child = fork();

        if(child == 0) {
            execv("/bin/mkdir", argv3);
        }
    }
}
```
Fungsi `download_file()` digunakan untuk download file,setelah di download lalu diunzip dengan fungsi `unzip_or_create_dir()` serta membuat folder hasil

### B
#### Decode dan masukan hasilnya ke file baru
```
char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/* decodeblock - decode 4 '6-bit' characters into 3 8-bit binary bytes */
void decodeblock(unsigned char in[], char *clrstr) {
  unsigned char out[4];
  out[0] = in[0] << 2 | in[1] >> 4;
  out[1] = in[1] << 4 | in[2] >> 2;
  out[2] = in[2] << 6 | in[3] >> 0;
  out[3] = '\0';
  strncat(clrstr, out, sizeof(out));
}

void b64_decode(char *b64src, char *clrdst) {
  int c, phase, i;
  unsigned char in[4];
  char *p;

  clrdst[0] = '\0';
  phase = 0; i=0;
  while(b64src[i]) {
    c = (int) b64src[i];
    if(c == '=') {
      decodeblock(in, clrdst); 
      break;
    }
    p = strchr(b64, c);
    if(p) {
      in[phase] = p - b64;
      phase = (phase + 1) % 4;
      if(phase == 0) {
        decodeblock(in, clrdst);
        in[0]=in[1]=in[2]=in[3]=0;
      }
    }
    i++;
  }
}
```
Fungsi tersebut yang akan digunaka untuk decode
```
typedef struct FILES{
    char **words;
    int wordCount;
}FILES;
FILES *files;


void decodemusic()
{
    
   int num;
   FILES *file;
   //FILE *fptr;
   //files = (FILES *)malloc(sizeof(FILES));
   char path[PATH_MAX];
   char list[NAME_MAX];
   char listfile[NAME_MAX];
   struct dirent *filesInDirectory;
   DIR *directory;


    strcpy(path, "/home/rendi/modul3/music/");
    strcpy(list, path);
    strcat(list, "music.txt");
    


   directory = opendir(path);
   files = (FILES *)malloc(sizeof(FILES));
    if(directory == NULL){
        printf("not valid");
    }else{
        while((filesInDirectory = readdir(directory))){
           
            if(strcmp(filesInDirectory->d_name, ".") != 0 && strcmp(filesInDirectory->d_name, "..") != 0 ){
               // printf("%s\n", filesInDirectory->d_name);
                char listfile[PATH_MAX]="/home/rendi/modul3/music/";
               strcat(listfile,filesInDirectory->d_name);
               FILE *fp = fopen(listfile,"r");
               if (fp == NULL){
                    printf("Error: could not open file %s",filesInDirectory->d_name );
                    //return 1;
                    }
                    char str[101];
         while (fgets(str, 101, fp) != NULL) {
        fp = fopen(list, "a");
        //printf("%s\n", str);
        char mydst[1024] = "";
        b64_decode(str, mydst);
        printf("%s\n", mydst);
        fprintf(fp, "%s\n",mydst);
        }
    
        }
    }
    }
}

void decodequote()
{
    
   int num;
   FILES *file;
   //FILE *fptr;
   //files = (FILES *)malloc(sizeof(FILES));
   char path[PATH_MAX];
   char list[NAME_MAX];
   char listfile[NAME_MAX];
   struct dirent *filesInDirectory;
   DIR *directory;


    strcpy(path, "/home/rendi/modul3/quote/");
    strcpy(list, path);
    strcat(list, "quote.txt");
    


   directory = opendir(path);
   files = (FILES *)malloc(sizeof(FILES));
    if(directory == NULL){
        printf("not valid");
    }else{
        while((filesInDirectory = readdir(directory))){
           
            if(strcmp(filesInDirectory->d_name, ".") != 0 && strcmp(filesInDirectory->d_name, "..") != 0 ){
               // printf("%s\n", filesInDirectory->d_name);
                char listfile[PATH_MAX]="/home/rendi/modul3/quote/";
               strcat(listfile,filesInDirectory->d_name);
               FILE *fp = fopen(listfile,"r");
               if (fp == NULL){
                    printf("Error: could not open file %s",filesInDirectory->d_name );
                   // return 1;
                    }
                    char str[101];
         while (fgets(str, 101, fp) != NULL) {
        fp = fopen(list, "a");
        //printf("%s\n", str);
        char mydst[1024] = "";
        b64_decode(str, mydst);
        printf("%s\n", mydst);
        fprintf(fp, "%s\n",mydst);
        }
    
        }
    }
    }
}
```
Fungsi `decodemusic()` digunakan untuk decode file music sedangkan fungsi `decodequote()` digunakan untuk decode file quote

### C
#### Pindahkan ke folder hasil
```
void move_music(){
    DIR *dir;
    struct dirent *dp;
    
    char from[PATH_MAX];
    char dest[PATH_MAX];

    strcpy(from, "/home/rendi/modul3/music/");
    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){
            printf("%s\n",dp->d_name);
            if(strcmp(dp->d_name,"music.txt")==0){
                strcat(from,dp->d_name);
                strcpy(dest, "/home/rendi/modul3/hasil/");
                 char *argv[] = {"mv", from, dest, NULL};
                 execv("/bin/mv", argv);      
            }

        }
    }
}

void move_quote(){
    DIR *dir;
    struct dirent *dp;
    
    char from[PATH_MAX];
    char dest[PATH_MAX];

    strcpy(from, "/home/rendi/modul3/quote/");
    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){
            printf("%s\n",dp->d_name);
            if(strcmp(dp->d_name,"quote.txt")==0){
                strcat(from,dp->d_name);
                strcpy(dest, "/home/rendi/modul3/hasil/");
                 char *argv[] = {"mv", from, dest, NULL};
                 execv("/bin/mv", argv);      
            }

        }
    }
}
```
fungsi `move_music()` untuk memindahkan file music yang sudah didecode ke folder hasil sedangkan fungsi `move_quote()` untuk memindahkan file quote yang sudah didecode ke folder hasil

### D
#### Zip dengan password
```
char *password = "mihinomenestrendi";
void *zip(void *args) {
    pthread_t id = pthread_self();
    char *argv[] = {"zip", "-P", password, "-r", "hasil.zip", "hasil", NULL};

    if(pthread_self() == thread[7]) {
        printf("-----ZIPPING HASIL.zip------\n");

        child = fork();

        if(child == 0) {
            execv("/bin/zip", argv);
        }
    }
}
```
Fungsi di atas digunakan untuk zip folder hasil dengan password `mihinomenestrendi`
### E
#### Buat file lalu zip ulang
```
void *unzip_or_create_file(void *args) {
    pthread_t id = pthread_self();
    char *argv[] = {"unzip", "-o", "-P", password, "hasil.zip", NULL};

    if(pthread_equal(id, thread[8])) {
        printf("-----RE_UNZIP hasil.zip------\n");

        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv);
        }
    }

    if(pthread_equal(id, thread[9])) {
        printf("-----CREATE no.txt------\n");

        FILE *fp = fopen("no.txt", "w");
        char *str = "No";

        fprintf(fp,"%s", str);

        fclose(fp);
    }
}
```
Pertama unzip dulu dengan password lalu create file no.txt
```
void *final_combine(void *args) {
    if(pthread_equal(pthread_self(), thread[10])) {
        printf("-----FINAL COMBINE------\n");

        child = fork();

        if(child == 0) {
            char *argv[] = {"zip", "-P", password, "hasil.zip", "hasil", "no.txt", NULL};
            execv("/bin/zip", argv);
        }
    }
}
```
lalu zip ulang dengan password

## Kendala dan kesulitan
- kesulitan dalam membuat thread


# Soal 2
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

### A
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
Username unique (tidak boleh ada user yang memiliki username yang sama)
Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil

#### Menghubungkan antara client dan server
##### Pada client

```
int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;

    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    char input[1024], name[1024], password[1024], temp;
    printf("Menghubungkan..\n\n");
```

Pada fungsi main dibuat beberapa fungsi untuk membuat socket antar client dan server. Fungsi `socket()` digunakan untuk mengecek apakah pembuatan socket berhasil. Lalu dicek juga apakah address yang dituju valid dan koneksi dari socket berhasil dengan menggunakan fungsi `inet_pton()` dan `connect()`.

```
valread = read(sock, buffer, 1024);
    while(strcmp("ready", buffer) != 0){
        printf("Masukan sesuatu untuk reload...");
        scanf("%s", input);
        send(sock, input, strlen(input), 0);
        valread = read(sock, buffer, 1024);
    }

     memset(buffer, 0, 1024);
    printf("Terhubung\n");


```

Pada program `client` akan meminta input untuk dikirimkan pada server dan jika dari server sudah mengirimkan char "ready" yang ditampung pada char `buffer`. Maka sudah dipastikan client sudah terhubung pada server.

##### Pada Server
```
int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    while (new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) {
        printf("Client tersambung dengan server...\n\n");
        pthread_create(&tid, NULL, &process, &new_socket);

        if(new_socket < 0){
            perror("accept");
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}
```

Dibuat beberapa perintah untuk membuat socket dengan client. Untuk penyimpanan data yang dikirim client nantinya akan di proses dalam fungsi `process` yang dijalankan dengan sebuah thread.

```
void *process(void *temp){
    Account account;
    FILE *file;

    int new_socket = *(int *) temp;
    int valread;
    char buffer[1024] = {0};

    // client hanlder, single-connection or multiconnection
    send(new_socket, "unready", 1024, 0);
    while(connection){
        valread = read(new_socket, buffer, 1024);
        send(new_socket, "unready", 1024, 0);
    }

    connection = 1;

    valread = read(new_socket, buffer, 1024);
    send(new_socket, "ready", 1024, 0);
```

Pada awal fungsi `process` akan program client dan server akan saling send dan read untuk memastikan connectionnya. 


#### Login 
##### Pada Client

```
    while(true){
        printf("Menu: \n1. Login \n2. Register \nq. Quit\n\n");
        printf("Pilih menu: ");
        scanf("%s", input);

        // Login
        if(strcmp(input, "1") == 0){
            send(sock, "login", 1024, 0);

            // input username
            // send request to server
            printf("Login\n");
            printf("Username: ");
            scanf("%c", &temp);
            scanf("%[^\n]", name);
            send(sock, name, strlen(name), 0);

            // input password
            printf("Password: ");
            scanf("%c", &temp);
            scanf("%[^\n]", password);
            send(sock, password, strlen(password), 0);
            memset(buffer, 0, 1024);

            // response from server
            valread = read(sock, buffer, 1024);
            if(strcmp(buffer, "success") == 0){
                printf("\nKamu diterima maszzeehh!\n\n");


```
Saat sudah terhubung, pada client memberikan pilihan untuk login, register, atau quit. Jika client ingin melakukan login dimana menginputkan `1` makan client akan mengirim request login ke server dengan menyertakan username dan password dari client.

Respon dari server apakah login success atau gagal akan di read oleh program client setelah password diinputkan

```
while(true){
                    printf("1. Add\n2. See\n3. Download\n4. Submit\n q. Logout\n\n");
                    printf("Pilih menu: ");
                    scanf("%s", input);

```
Setelah login success maka client dapat memilih beberapa command-command yang ada.

##### Pada server
```
while(true){
        printf("Connected to client\n\n");
        valread = read(new_socket, buffer, 1024);

        // Login
        if(strcmp(buffer, "login") == 0){
            valread = read(new_socket, account.name, 1024);
            valread = read(new_socket, account.password, 1024);

            // check file already exist
            if(access("users.txt", F_OK) != 0){
                printf("File not found. Creating file.... \n\n");
                file = fopen("users.txt", "a+");
                fclose(file);
            }

            file = fopen("users.txt", "a+")
            

```

Server akan mengecek request client. Jika client request untuk login, server menampung name dan password pada struct `account`. Selanjutnya jika file user belum ada maka akan dibuat file user dan akan dibuka oleh server.

```
 // check username and password
            int flag = 0;
            char *line = NULL;
            ssize_t len = 0;
            ssize_t file_read;

            while((file_read = getline(&line, &len, file) != -1)) {
                char t_name[1024];
                char t_pass[1024];

                // username
                int i = 0;
                while(line[i] != ':'){
                    t_name[i] = line[i];
                    i++;
                }

                t_name[i] = '\0';
                i++;

                // password
                int j = 0;
                while(line[i] != '\n'){
                    t_pass[j] = line[i];
                    i++;
                    j++;
                }

                t_pass[j] = '\0';
                j++;

                if(strcmp(account.name, t_name) == 0 && strcmp(account.password, t_pass) == 0){
                    flag = 1;
                    send(new_socket, "success", 1024, 0);
                    break;
                }
            }

```
Server akan membaca file per-barisnya dan memasukan user dalam text pada variabel `t_name` serta password pada `t_pass`. Pada akhir while loop akan dicek apakah username dari client ada. Jika ada dimana ditandai dengan variabel `flag` yang di assign nilai 1, maka server akan mengirim pesan login sukses.

###### Login gagal
```
// attemp logim failed
            if(flag == 0){
                printf("Login failed...\n\n");
                send(new_socket, "error", 1024, 0);
            }
```
Jika login gagal dimana flag masih bernilai 0, maka server mengirim pesan login error


#### Register
##### Pada Client
```
// Register
        else if(strcmp(input, "2") == 0){
            send(sock, "register", 1024, 0);

            // input username
            // send request to server
            printf("Register\n");
            printf("Username: ");
            scanf("%c", &temp);
            scanf("%[^\n]", name);
            send(sock, name, strlen(name), 0);

            // input password
            printf("Password: ");
            scanf("%c", &temp);
            scanf("%[^\n]", password);
            send(sock, password, strlen(password), 0);

```

Jika pada client diberikan input 2 maka, client akan send register request ke server dengan mengirimkan pula username dan password yang akan dibuat client

```
  // response from server
            valread = read(sock, buffer, 1024);
            if(strcmp(buffer, "error") == 0){
                printf("\nDia sudah ada yg memiliki atau passwordmu tidak sesuai type nya!!\n");
            }else if(strcmp(buffer, "success") == 0){
                printf("\nRegister berhasil! Selamat, kamu dapat melanjutkan hubunganmu:)\n");
            }
            
            memset(buffer, 0, 1024);
        }
```
Pada client akan menampilkan pesan jika terjadi error atau register berhasil.

##### Pada Server
```
 // Register
        if(strcmp(buffer, "register") == 0){

            // check file already exist
            if(access("users.txt", F_OK) != 0){
                printf("File not found. Creating file.... \n\n");
                file = fopen("users.txt", "a+");
                fclose(file);
            }

            file = fopen("users.txt", "a+");

            valread = read(new_socket, account.name, 1024);
            valread = read(new_socket, account.password, 1024);

            // check exist username
            int flag = 0;
            char *line = NULL;
            ssize_t len = 0;
            ssize_t file_read;

            while((file_read = getline(&line, &len, file) != -1)) {
                char t_name[1024];

                int i = 0;
                while(line[i] != ':'){
                    t_name[i] = line[i];
                    i++;
                }

                t_name[i] = '\0';
                i++;

                if(strcmp(account.name, t_name) == 0){
                    flag = 1;
                    break;
                }
            }
```
Pada fungsi `process()` akan dijalankan proses register jika client mengirimkan pesan register. Server akan mengecek apakah file txt dari user sudah ada, jika belum maka server akan membuat filenya. Selanjutnya server akan menerima username dan password dari client dan mengecek username apakah sudah ada di file user.

```
// check password 
            // 1 lowercase, 1 uppercase and 1 number
            int upper = 0, lower = 0, number = 0;
            for (int i = 0; i < strlen(account.password); i++){
                if(account.password[i] >= 'A' && account.password[i] <= 'Z') upper++;
                else if(account.password[i] >= 'a' && account.password[i] <= 'z') lower++;
                else if(account.password[i] >= '0' && account.password[i] <= '9') number++;
            }
```
Server akan mengecek pula apakah password untuk register ini sudah sesuai kriteria

```
// insert into file when username and password success check
            if(flag == 0 && upper >= 1 && lower >= 1 && number >= 1 && strlen(account.password) >= 6){
                // write file
                fprintf(file, "%s:%s\n", account.name, account.password);
                printf("%s account added successfully\n\n", account.name);

                send(new_socket, "success", 1024, 0);
            }else{
                send(new_socket, "error", 1024, 0);
            }
```
Jika User belum exist dan password sesuai kriteria, maka akan dikirimkan pesan success kepada client. Jika tidak memenuhi maka akan dikirim pesan error ke client.

### G
#### Soal
Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

#### Pada client
```
 if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    while (new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) {
        printf("Client tersambung dengan server...\n\n");
        pthread_create(&tid, NULL, &process, &new_socket);

        if(new_socket < 0){
            perror("accept");
            exit(EXIT_FAILURE);
        }
    }

```
multiple-connection dibuat dengan menggunakan `listen()` dan thread pada fungsi `process()` di server. Dimana Jika koneksi dibuat, thread utama akan membuat thread baru dan mengoper koneksinya kepada thread baru tersebut.


### No. 3

Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.
Contoh jika program pengkategorian dijalankan

#### A
##### Soal
Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif

##### Extract File

```
// extract zip file
void extract(char *value) {
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    file = popen(value ,"r");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(file);
    }
}
```
Pada fungsi `extract` akan mengekstract file dengan `popen` dimana value nantinya akan ada command unzip pada path file dari file hartakarun.zip


```
// sorting file by category
void sort_file(char *from){
    struct_path s_path;
    struct dirent *dp;
    DIR *dir;

    flag = 1;
    int index = 0;
    strcpy(s_path.cwd, "/home/wahid/shift3/hartakarun");

    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move_move, (void *)&s_path);
            sleep(1);
            index++;
        }
    }

    if(!flag){
        printf("Maaf kawan, lupakan saja :)\n");
    }else {
        printf("Kamu bisa lanjut, tapi jangan berharap lebih :)\n");
    }
}
```

Dengan menggunakan library `dirent`, program akan membaca apa saja yang ada di directory hingga NULL. Lalu akan dijalankan thread untuk fungsi `move_move` yang akan memindahkan file menuju folder yang sesuai formatnya

```
// move
void *move_move(void *s_path){
    struct_path s_paths = *(struct_path*) s_path;
    move_file(s_paths.from, s_paths.cwd);
    pthread_exit(0);
}

// move file
void move_file(char *p_from, char *p_cwd){
    char file_name[300], file_ext[30], temp[300];
    char *buffer;

    strcpy(temp, p_from);
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    convert_to_lower(file_ext);

    // new dest
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);

    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    create_dir(file_ext);
    rename(p_from, p_cwd);
}
```
Fungsi `move_move` akan memanggil fungsi `move_file` yang akan memindahkan file sesuai dengan format filenya. Pertama fungsi akan looping buffer hingga akhir dari "/" yang menandakan nama file. Nama file ditampung dan dikategorikan dengan formatnya. Folder dengan format file akan dibuat dengan fungsi `create_dir` dan file akan di pindahkan dengan fungsi `rename` yang akan memindahkan file lama ke file baru berdasarkan path yang sudah di buat di `p_cwd`

#### B
##### Soal
Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

##### Script
```
while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }
```
Untuk hidden file penamaannya diawali dengan dot maka akan dicek apakah char index pertamanya untuk file hidden. untuk file unknown akan tersortir setelah else if karena buffer menampung strtok file name setelah "." yang menandakan format file.


#### C
##### Soal
Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

##### Script
###### Fungsi `sort_file` pada soal3.c
```
while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move_move, (void *)&s_path);
            sleep(1);
            index++;
        }
```
Di fungsi `sort_file` terdapat while loop yang mengiterasi file dalam directory hartakarun. Pada setiap iterasi akan dibuat 1 thread untuk yang memanggil fungsi `move_move` untuk memindahkan file.

#### D
##### Soal
Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.

##### Fungsi Zip
```
zip("zip -o -r /home/wahid/shift3/Client/hartakarun.zip ../hartakarun");

void zip(char *ziping){
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer, '\0', sizeof(buffer));

    //ziping
    file = popen(ziping, "w");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n", buffer);
        pclose(file);
    }
```

Pada program client, fungsi `zip()` akan menerima parameter barupa command yang akan dieksekusi dalam terminal. Pada fungsi main fungsi zip akan menerima parameter command zip pada file path hartakarun. Command ini selanjutnya akan dieksekusi dengan popen yang ada dalam fungsi `zip`.

#### Dokumentasi Pengerjaan no.3
##### Sebelum Dieksekusi
![messageImage_1650108719873](/uploads/c2c0c8e1903b6e49bcc9085a3d492c12/messageImage_1650108719873.jpg)
##### Setelah Dieksekusi
![messageImage_1650108721901](/uploads/21be2d1af373fc817a439a8b5f300d3c/messageImage_1650108721901.jpg)
