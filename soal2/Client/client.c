#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <ctype.h>
#include <termios.h>
#include <pthread.h>
#include <stdbool.h>
#include <fcntl.h>

#define PORT 8080
  

/*
    Author   : Abd. Wahid
    NRP      : 5025201039
    kelompok : C07

    note: nyebar tanpa ijin pembuat, perutnya isi paku
*/
int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;

    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    char input[1024], name[1024], password[1024], temp;
    printf("Menghubungkan..\n\n");

    valread = read(sock, buffer, 1024);
    while(strcmp("ready", buffer) != 0){
        printf("Masukan sesuatu untuk reload...");
        scanf("%s", input);
        send(sock, input, strlen(input), 0);
        valread = read(sock, buffer, 1024);
    }

    memset(buffer, 0, 1024);
    printf("Terhubung\n");

    while(true){
        printf("Menu: \n1. Login \n2. Register \nq. Quit\n\n");
        printf("Pilih menu: ");
        scanf("%s", input);

        // Login
        if(strcmp(input, "1") == 0){
            send(sock, "login", 1024, 0);

            // input username
            // send request to server
            printf("Login\n");
            printf("Username: ");
            scanf("%c", &temp);
            scanf("%[^\n]", name);
            send(sock, name, strlen(name), 0);

            // input password
            printf("Password: ");
            scanf("%c", &temp);
            scanf("%[^\n]", password);
            send(sock, password, strlen(password), 0);
            memset(buffer, 0, 1024);

            // response from server
            valread = read(sock, buffer, 1024);
            if(strcmp(buffer, "success") == 0){
                printf("\nKamu diterima maszzeehh!\n\n");

                while(true){
                    printf("1. Add\n2. See\n3. Download\n4. Submit\n q. Logout\n\n");
                    printf("Pilih menu: ");
                    scanf("%s", input);

                    // Add problems
                    if(strcmp(input, "1") == 0){
                        send(sock, "add", 1024, 0);
                        char data[1024];
                        printf("\n---Add---\n");

                        printf("Judul problem: ");
                        scanf("%c", &temp);
                        scanf("%[^\n]", data);
                        send(sock, data, strlen(data), 0);

                        printf("Filepath description.txt: ");
                        scanf("%c", &temp);
                        scanf("%[^\n]", data);
                        send(sock, data, strlen(data), 0);

                        // printf("Filepath input.txt: ");
                        // scanf("%c", &temp);
                        // scanf("%[^\n]", data);
                        // send(sock, data, strlen(data), 0);

                        // printf("Filepath output.txt: ");
                        // scanf("%c", &temp);
                        // scanf("%[^\n]", data);
                        // send(sock, data, strlen(data), 0);

                        int file = open(data, O_RDONLY);
                        if(!file){
                            perror("can't open");
                            exit(EXIT_FAILURE);
                        }

                        int read_len;
                        while(true){
                            memset(data, 0x00, 1024);
                            read_len = read(file, data, 1024);

                            if(read_len == 0) break;
                            else send(sock, data, read_len, 0);
                        }

                        close(file);
                        printf("Add Success...\n\n");
                        continue;
                    }

                    // See
                    else if(strcmp(input, "2") == 0){
                        send(sock, "see", 1024, 0);

                    }

                    // Download
                    else if(strcmp(input, "3") == 0){
                        send(sock, "download", 1024, 0);

                    }

                    // Submit
                    else if(strcmp(input, "4") == 0){
                        send(sock, "submit", 1024, 0);

                    }

                    // Quit
                    else if(strcmp(input, "q") == 0){
                        printf("Berhasil logout....\n\n");
                        send(sock, "logout", 1024, 0);
                        break;
                    }
                }

            }else if(strcmp(buffer, "error") == 0){
                printf("\nMaaf, kamu tertolak kawan :( \n\n");
            }

            memset(buffer, 0, 1024);
        }
        
        // Register
        else if(strcmp(input, "2") == 0){
            send(sock, "register", 1024, 0);

            // input username
            // send request to server
            printf("Register\n");
            printf("Username: ");
            scanf("%c", &temp);
            scanf("%[^\n]", name);
            send(sock, name, strlen(name), 0);

            // input password
            printf("Password: ");
            scanf("%c", &temp);
            scanf("%[^\n]", password);
            send(sock, password, strlen(password), 0);

            // response from server
            valread = read(sock, buffer, 1024);
            if(strcmp(buffer, "error") == 0){
                printf("\nDia sudah ada yg memiliki atau passwordmu tidak sesuai type nya!!\n");
            }else if(strcmp(buffer, "success") == 0){
                printf("\nRegister berhasil! Selamat, kamu dapat melanjutkan hubunganmu:)\n");
            }
            
            memset(buffer, 0, 1024);
        }

        // Quit
        else if(strcmp(input, "q") == 0){
            send(sock, input, strlen(input), 0);
            printf("closing\n");
            return 0;
        }else{
            printf("Tidak dapat memahamimu :( ...\n\n");
        }

        memset(buffer, 0, 1024);
        send(sock, "return", 1024, 0);
    }

    return 0;
}