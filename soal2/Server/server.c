#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <pthread.h>
#include <stdbool.h>
#include <fcntl.h>

#define PORT 8080

pthread_t tid;
int connection = 0;

// save users
typedef struct account{
    char name[64];
    char password[64];
} Account;

typedef struct problems{
    char title[100];
    char description[100];
    char input[100];
    char output[100];
} Problems;


void *process(void *temp);

/*
    Author   : Abd. Wahid
    NRP      : 5025201039
    kelompok : C07

    note: nyebar tanpa ijin pembuat, perutnya isi paku
*/

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    while (new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) {
        printf("Client tersambung dengan server...\n\n");
        pthread_create(&tid, NULL, &process, &new_socket);

        if(new_socket < 0){
            perror("accept");
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}

void *process(void *temp){
    Account account;
    FILE *file;

    int new_socket = *(int *) temp;
    int valread;
    char buffer[1024] = {0};

    // client hanlder, single-connection or multiconnection
    send(new_socket, "unready", 1024, 0);
    while(connection){
        valread = read(new_socket, buffer, 1024);
        send(new_socket, "unready", 1024, 0);
    }

    connection = 1;

    valread = read(new_socket, buffer, 1024);
    send(new_socket, "ready", 1024, 0);

    while(true){
        printf("Connected to client\n\n");
        valread = read(new_socket, buffer, 1024);

        // Login
        if(strcmp(buffer, "login") == 0){
            valread = read(new_socket, account.name, 1024);
            valread = read(new_socket, account.password, 1024);

            // check file already exist
            if(access("users.txt", F_OK) != 0){
                printf("File not found. Creating file.... \n\n");
                file = fopen("users.txt", "a+");
                fclose(file);
            }

            file = fopen("users.txt", "a+");

            // check username and password
            int flag = 0;
            char *line = NULL;
            ssize_t len = 0;
            ssize_t file_read;

            while((file_read = getline(&line, &len, file) != -1)) {
                char t_name[1024];
                char t_pass[1024];

                // username
                int i = 0;
                while(line[i] != ':'){
                    t_name[i] = line[i];
                    i++;
                }

                t_name[i] = '\0';
                i++;

                // password
                int j = 0;
                while(line[i] != '\n'){
                    t_pass[j] = line[i];
                    i++;
                    j++;
                }

                t_pass[j] = '\0';
                j++;

                if(strcmp(account.name, t_name) == 0 && strcmp(account.password, t_pass) == 0){
                    flag = 1;
                    send(new_socket, "success", 1024, 0);
                    break;
                }
            }

            fclose(file);

            // attemp logim failed
            if(flag == 0){
                printf("Login failed...\n\n");
                send(new_socket, "error", 1024, 0);
            }

            // when account axist
            else{
                printf("Login success...\n\n");
                
                while(true){
                    valread = read(new_socket, buffer, 1024);
                    
                    if(strcmp(buffer, "add") == 0){
                        Problems request;

                        // char clientPath[1024];

                        // valread = read(new_socket, request.title, 1024);
                        // valread = read(new_socket, request.description, 1024);

                        // mkdir("problems", 0777);
                        // strcpy(request.description, "/home/wahid/sisop/modul3/prak/soal2/Server/problems/");
                        // strcat(request.description, request.title);

                        // // start adding
                        // int dest_desc = open(request.description, O_WRONLY | O_CREAT | O_EXCL, 0700);
                        // if(!dest_desc) {
                        //     perror("can't open");
                        //     exit(EXIT_FAILURE);
                        // }

                        // int desc_read_len;
                        // char buff_desc[1024];

                        // while(true){
                        //     memset(buff_desc, 0x00, 1024);
                        //     write(dest_desc, buff_desc, desc_read_len);
                        //     break;
                        // }

                        // file = fopen("problems.tsv", "a");
                        // fprintf(file, "%s\t%s\n", request.title, account.name);
                        // fclose(file);
                        // continue;
                    }

                    // See
                    else if(strcmp(buffer, "see") == 0){

                    }

                    // Download
                    else if(strcmp(buffer, "download") == 0){

                    }

                    // Submit
                    else if(strcmp(buffer, "submit") == 0){

                    }

                    // Quit
                    else if(strcmp(buffer, "logout") == 0){
                        printf("Berhasil logout....\n\n");
                        break;
                    }
                }
            }
        }

        // Register
        if(strcmp(buffer, "register") == 0){

            // check file already exist
            if(access("users.txt", F_OK) != 0){
                printf("File not found. Creating file.... \n\n");
                file = fopen("users.txt", "a+");
                fclose(file);
            }

            file = fopen("users.txt", "a+");

            valread = read(new_socket, account.name, 1024);
            valread = read(new_socket, account.password, 1024);

            // check exist username
            int flag = 0;
            char *line = NULL;
            ssize_t len = 0;
            ssize_t file_read;

            while((file_read = getline(&line, &len, file) != -1)) {
                char t_name[1024];

                int i = 0;
                while(line[i] != ':'){
                    t_name[i] = line[i];
                    i++;
                }

                t_name[i] = '\0';
                i++;

                if(strcmp(account.name, t_name) == 0){
                    flag = 1;
                    break;
                }
            }
            

            // check password 
            // 1 lowercase, 1 uppercase and 1 number
            int upper = 0, lower = 0, number = 0;
            for (int i = 0; i < strlen(account.password); i++){
                if(account.password[i] >= 'A' && account.password[i] <= 'Z') upper++;
                else if(account.password[i] >= 'a' && account.password[i] <= 'z') lower++;
                else if(account.password[i] >= '0' && account.password[i] <= '9') number++;
            }

            // insert into file when username and password success check
            if(flag == 0 && upper >= 1 && lower >= 1 && number >= 1 && strlen(account.password) >= 6){
                // write file
                fprintf(file, "%s:%s\n", account.name, account.password);
                printf("%s account added successfully\n\n", account.name);

                send(new_socket, "success", 1024, 0);
            }else{
                send(new_socket, "error", 1024, 0);
            }

            fclose(file);

        } else if(strcmp(buffer, "quit") == 0){
            printf("Logout.. \n\n");
            connection = 0;
            close(new_socket);
            break;
        }else if(strcmp(buffer, "return") == 0){
            printf("Reconnecting... \n\n");
            continue;
        }
    }
}