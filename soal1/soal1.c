#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<pthread.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include<dirent.h>

pthread_t thread[11];
pid_t child;
char *password = "mihinomenestrendi";

void download_file(){
    char *files[] = {
        "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "music.zip",
        "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "quote.zip"
    };

    int status;
    pid_t child = fork();

    if(child < 0) exit(EXIT_FAILURE);

    for (int i = 0; i < 4; i += 2){
        if(child == 0){
            char *argv[] = {"wget", "-q", "--no-check-certificate", files[i], "-O", files[i + 1], NULL};
            execv("/bin/wget", argv);
        }
        while((wait(&status)) > 0);
        child = fork();
    }
}

char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/* decodeblock - decode 4 '6-bit' characters into 3 8-bit binary bytes */
void decodeblock(unsigned char in[], char *clrstr) {
  unsigned char out[4];
  out[0] = in[0] << 2 | in[1] >> 4;
  out[1] = in[1] << 4 | in[2] >> 2;
  out[2] = in[2] << 6 | in[3] >> 0;
  out[3] = '\0';
  strncat(clrstr, out, sizeof(out));
}

void b64_decode(char *b64src, char *clrdst) {
  int c, phase, i;
  unsigned char in[4];
  char *p;

  clrdst[0] = '\0';
  phase = 0; i=0;
  while(b64src[i]) {
    c = (int) b64src[i];
    if(c == '=') {
      decodeblock(in, clrdst); 
      break;
    }
    p = strchr(b64, c);
    if(p) {
      in[phase] = p - b64;
      phase = (phase + 1) % 4;
      if(phase == 0) {
        decodeblock(in, clrdst);
        in[0]=in[1]=in[2]=in[3]=0;
      }
    }
    i++;
  }
}


typedef struct FILES{
    char **words;
    int wordCount;
}FILES;
FILES *files;


void decodemusic()
{
    
   int num;
   FILES *file;
   //FILE *fptr;
   //files = (FILES *)malloc(sizeof(FILES));
   char path[PATH_MAX];
   char list[NAME_MAX];
   char listfile[NAME_MAX];
   struct dirent *filesInDirectory;
   DIR *directory;


    strcpy(path, "/home/rendi/modul3/music/");
    strcpy(list, path);
    strcat(list, "music.txt");
    


   directory = opendir(path);
   files = (FILES *)malloc(sizeof(FILES));
    if(directory == NULL){
        printf("not valid");
    }else{
        while((filesInDirectory = readdir(directory))){
           
            if(strcmp(filesInDirectory->d_name, ".") != 0 && strcmp(filesInDirectory->d_name, "..") != 0 ){
               // printf("%s\n", filesInDirectory->d_name);
                char listfile[PATH_MAX]="/home/rendi/modul3/music/";
               strcat(listfile,filesInDirectory->d_name);
               FILE *fp = fopen(listfile,"r");
               if (fp == NULL){
                    printf("Error: could not open file %s",filesInDirectory->d_name );
                    //return 1;
                    }
                    char str[101];
         while (fgets(str, 101, fp) != NULL) {
        fp = fopen(list, "a");
        //printf("%s\n", str);
        char mydst[1024] = "";
        b64_decode(str, mydst);
        printf("%s\n", mydst);
        fprintf(fp, "%s\n",mydst);
        }
    
        }
    }
    }
}

void decodequote()
{
    
   int num;
   FILES *file;
   //FILE *fptr;
   //files = (FILES *)malloc(sizeof(FILES));
   char path[PATH_MAX];
   char list[NAME_MAX];
   char listfile[NAME_MAX];
   struct dirent *filesInDirectory;
   DIR *directory;


    strcpy(path, "/home/rendi/modul3/quote/");
    strcpy(list, path);
    strcat(list, "quote.txt");
    


   directory = opendir(path);
   files = (FILES *)malloc(sizeof(FILES));
    if(directory == NULL){
        printf("not valid");
    }else{
        while((filesInDirectory = readdir(directory))){
           
            if(strcmp(filesInDirectory->d_name, ".") != 0 && strcmp(filesInDirectory->d_name, "..") != 0 ){
               // printf("%s\n", filesInDirectory->d_name);
                char listfile[PATH_MAX]="/home/rendi/modul3/quote/";
               strcat(listfile,filesInDirectory->d_name);
               FILE *fp = fopen(listfile,"r");
               if (fp == NULL){
                    printf("Error: could not open file %s",filesInDirectory->d_name );
                   // return 1;
                    }
                    char str[101];
         while (fgets(str, 101, fp) != NULL) {
        fp = fopen(list, "a");
        //printf("%s\n", str);
        char mydst[1024] = "";
        b64_decode(str, mydst);
        printf("%s\n", mydst);
        fprintf(fp, "%s\n",mydst);
        }
    
        }
    }
    }
}

void move_music(){
    DIR *dir;
    struct dirent *dp;
    
    char from[PATH_MAX];
    char dest[PATH_MAX];

    strcpy(from, "/home/rendi/modul3/music/");
    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){
            printf("%s\n",dp->d_name);
            if(strcmp(dp->d_name,"music.txt")==0){
                strcat(from,dp->d_name);
                strcpy(dest, "/home/rendi/modul3/hasil/");
                 char *argv[] = {"mv", from, dest, NULL};
                 execv("/bin/mv", argv);      
            }

        }
    }
}

void move_quote(){
    DIR *dir;
    struct dirent *dp;
    
    char from[PATH_MAX];
    char dest[PATH_MAX];

    strcpy(from, "/home/rendi/modul3/quote/");
    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){
            printf("%s\n",dp->d_name);
            if(strcmp(dp->d_name,"quote.txt")==0){
                strcat(from,dp->d_name);
                strcpy(dest, "/home/rendi/modul3/hasil/");
                 char *argv[] = {"mv", from, dest, NULL};
                 execv("/bin/mv", argv);      
            }

        }
    }
}


void* download(void *arg)
{
	pthread_t id = pthread_self();
       
     if(pthread_equal(id, thread[0])) {
        child = fork();

        if(child == 0) {
           download_file();
        }

}
}


void *unzip_or_create_dir(void *args) {
    char *argv1[] = {"unzip", "-o", "music.zip", "-d", "music", NULL};
    char *argv2[] = {"unzip", "-o", "quote.zip", "-d", "quote", NULL};
    char *argv3[] = {"mkdir", "-p", "hasil", NULL};


    pthread_t id = pthread_self();
    int status;

    if(pthread_equal(id, thread[1])) {
        printf("-----UNZIP MUSIC------\n");
        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv1);
        }
    }

    if(pthread_equal(id, thread[2])) {
        printf("-----UNZIP QUOTE------\n");
        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv2);
        }
    }

    if(pthread_equal(id, thread[3])) {
        printf("-----CREATE FILE------\n");
        child = fork();

        if(child == 0) {
            execv("/bin/mkdir", argv3);
        }
    }
}


void *decoding(void *args) {
    pthread_t id = pthread_self();

    if(pthread_equal(id, thread[4])) {
        printf("-----DECODING MUSIC------\n");
        decodemusic();
    }

    if(pthread_equal(id, thread[5])) {
        printf("-----DECODING QUOTE------\n");
        decodequote();
    }

    return NULL;
}

void* movelist(void *arg)
{
	pthread_t id = pthread_self();
       
     if(pthread_equal(id, thread[6])) {
        child = fork();

        if(child == 0) {
           move_music();
        }else
            move_quote();

    }

}

void *zip(void *args) {
    pthread_t id = pthread_self();
    char *argv[] = {"zip", "-P", password, "-r", "hasil.zip", "hasil", NULL};

    if(pthread_self() == thread[7]) {
        printf("-----ZIPPING HASIL.zip------\n");

        child = fork();

        if(child == 0) {
            execv("/bin/zip", argv);
        }
    }
}

void *unzip_or_create_file(void *args) {
    pthread_t id = pthread_self();
    char *argv[] = {"unzip", "-o", "-P", password, "hasil.zip", NULL};

    if(pthread_equal(id, thread[8])) {
        printf("-----RE_UNZIP hasil.zip------\n");

        child = fork();

        if(child == 0) {
            execv("/bin/unzip", argv);
        }
    }

    if(pthread_equal(id, thread[9])) {
        printf("-----CREATE no.txt------\n");

        FILE *fp = fopen("no.txt", "w");
        char *str = "No";

        fprintf(fp,"%s", str);

        fclose(fp);
    }
}

void *final_combine(void *args) {
    if(pthread_equal(pthread_self(), thread[10])) {
        printf("-----FINAL COMBINE------\n");

        child = fork();

        if(child == 0) {
            char *argv[] = {"zip", "-P", password, "hasil.zip", "hasil", "no.txt", NULL};
            execv("/bin/zip", argv);
        }
    }
}

int main() {
   


    //zip
    for(int i = 7; i < 8; ++i) {
        pthread_create(&thread[i], NULL, &zip, NULL);
        pthread_join(thread[i], NULL);
    }

    sleep(1);

    // unzip or create file
    for(int i = 8; i < 10; ++i) {
        pthread_create(&thread[i], NULL, &unzip_or_create_file, NULL);
    }

    for(int i = 8; i < 10; ++i) {
        pthread_join(thread[i], NULL);
    }

    sleep(1);

    // final combine
    for(int i = 10; i < 11 ; ++i) {
        pthread_create(&thread[i], NULL, &final_combine, NULL);
        pthread_join(thread[i], NULL);
    }
    
    pthread_exit(thread);
    exit(0);
    return 0;
}
